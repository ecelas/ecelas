# Ecelas

Ecelas is a future alternative web client for [Mastodon](https://github.com/tootsuite/mastodon).

## Plans

### Goals

- Multi-instance support
- Accessibility
- Multiple languages (i18n)
- Admin/moderation support
- Basic support when JS is disabled
- Fast on low end devices
- Small page
- Many themes
- Good audio and recording support
- Auto light and dark mode
- LibreJS support

### Secondary Goals

- Extra emojis
- Translating toots in foreign languages

### Not Goals

- Native applications (like Android/iOS or Electron)

### Technology

I am still working on it, but here are some of the basics:
- JavaScript for client-side code
- - Yarn for dealing with node_modules
- - StandardJS

## Name

I came up with the name, and [looked it up](https://en.wiktionary.org/wiki/ecelar). I think this program is going to be excellent, so the name fits.
